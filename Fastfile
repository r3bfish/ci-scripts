
require 'json'
require 'uri'
require 'net/http'


#
# iOS
#

platform :ios do
  before_all do
    # ewa hardcoded-specifics
    ENV['TARGET_EWA'] = 'EWA'
    ENV['TARGET_EWA_WATCHAPP'] = 'EWA_Apple_Watch'
    ENV['TARGET_EWA_WATCHAPP_EXT'] = 'EWA_Apple_Watch Extension'
  end

  #
  # Lanes
  #

  desc 'Run tests'
  lane :ci_tests do |options|
    cocoapods(try_repo_update_on_error: true)
    run_tests(scheme: options[:scheme], slack_only_on_failure: true)
  end

  desc 'Run tests on AWS device farm'
  lane :ci_aws_tests do |options|
    xcodebuild(
      scheme: options[:scheme],
      destination: 'generic/platform=iOS',
      configuration: options[:configuration],
      derivedDataPath: 'aws',
      xcargs: "build-for-testing GCC_PREPROCESSOR_DEFINITIONS='$(inherited) DEBUG=1 AWS_UI_TEST=1'"
    )

    aws_device_farm_package(
      derrived_data_path: 'aws',
      configuration: options[:configuration]
    )

    aws_device_farm(
      name: 'appewa',
      device_pool: options[:device_pool],
      test_type: 'XCTEST_UI'
    )
  end

  desc 'Ensure that changelog entries are ready'
  lane :ci_changelog do |options|
    destination = ENV['APPCENTER_GROUP']
    notes = changelog_for_ios(options[:target])
    if notes.nil? and destination != 'Internal'
      puts('Add changelog entries to CHANGELOG.md!')
      puts('Use [Unreleased] section for testing or internal builds.')
      puts('Use [<version aka future release tag>] section for release builds.')
      exit(1)
    end
    puts "\nRelease notes:\n#{notes}"
  end

  desc 'Build iOS app using provided parameters'
  lane :ci_build_app do |options|
    exit_if_not_ci()

    build_ios_app(
      options[:target],
      options[:match_type],
      options[:signing_type],
      options[:icloud_env],
      options[:scheme]
    )
  end

  desc 'Deploy build, for testing or internal use only'
  lane :ci_deploy_app do |options|
    exit_if_not_ci()
    destination = ENV['APPCENTER_GROUP']
    notes = changelog_for_ios(options[:target])
    puts "\nRelease notes:\n#{notes}"
    deploy_ios_to_appcenter(
      options[:target],
      notes,
      destination,
      options[:browse_url]
    )
  end

  desc 'Build release build'
  lane :ci_release_build_appcenter do |options|
    exit_if_not_ci()
    release_build_appcenter(
      options[:target],
      options[:scheme]
    )
  end

  desc 'Deploy release build to AppCenter'
  lane :ci_release_deploy_appcenter do |options|
    exit_if_not_ci()
    release_deploy_appcenter(
      options[:target],
      options[:scheme],
      options[:browse_url]
    )
  end

  desc 'Export release build to appstore'
  lane :ci_release_build_appstore do |options|
    exit_if_not_ci()
    release_build_appstore(
      options[:target],
      options[:scheme]
    )
  end

  desc 'Submit release build to AppStore'
  lane :ci_release_submit_appstore do |options|
    exit_if_not_ci()
    release_submit_appstore(
      options[:target],
      options[:scheme]
    )
  end


  #
  # Build
  #

  def build_ios_app(target, match_type, signing_type, icloud_env, scheme)
    # prepare xcode
    xcode_select ENV['XCODE_APP']
    sh("mkdir -p #{ENV['ARCHIVES_DIR']}")
    sh("mkdir -p #{ENV['DERIVED_DATA_DIR']}")

    # update dependencies
    cocoapods(try_repo_update_on_error: true)

    # update build version
    build_number = build_number_for_branch(target)
    increment_build_number(build_number: build_number) if !build_number.nil?
    version_number = get_version_number(target: target)
    #fit_ios_badge(version_number)

    # code signing
    match(type: match_type, readonly: true, force_for_new_devices: true)

    update_provisions(target, match_type)

    # build
    gym(
      scheme: scheme,
      silent: true,
      clean: true,
      derived_data_path: ENV['DERIVED_DATA_DIR'],
      archive_path: ENV['ARCHIVES_DIR'],
      output_name: "#{target}.ipa",
      export_method: signing_type,
      export_options: {
        method: signing_type,
        iCloudContainerEnvironment: icloud_env,
        provisioningProfiles: ENV['MATCH_PROVISIONING_PROFILE_MAPPING']
      }
    )
  end

  def update_provisions(target, build_type)
    case target
    when 'EWA'
      update_code_signing_settings(
        targets: [ENV['TARGET_EWA']],
        profile_name: ENV["sigh_com.ewa.ewaapp_#{build_type}_profile-name"],
        code_sign_identity: 'iPhone Distribution'
      )
      update_code_signing_settings(
        targets: [ENV['TARGET_EWA_WATCHAPP']],
        profile_name: ENV["sigh_com.ewa.ewaapp.watchkitapp_#{build_type}_profile-name"],
        code_sign_identity: 'iPhone Distribution'
      )
      update_code_signing_settings(
        targets: [ENV['TARGET_EWA_WATCHAPP_EXT']],
        profile_name: ENV["sigh_com.ewa.ewaapp.watchkitapp.watchkitextension_#{build_type}_profile-name"],
        code_sign_identity: 'iPhone Distribution'
      )

    when 'EWA Kids'
      update_code_signing_settings(
        profile_name: ENV["sigh_com.ewa.ewakids_#{build_type}_profile-name"],
        code_sign_identity: 'iPhone Distribution'
      )

    when 'EyeTraining'
      update_code_signing_settings(
        profile_name: ENV["sigh_com.lithiumlab.vision_#{build_type}_profile-name"],
        code_sign_identity: 'iPhone Distribution'
      )

    else
      # nothing

    end
  end


  #
  # Deploy
  #

  def deploy_ios_to_appcenter(target, release_notes, destination, browse_url)
    is_internal = destination.downcase == 'internal'
    appcenter_upload(
      release_notes: release_notes,
      notify_testers: is_internal,
      file: "#{target}.ipa",
      dsym: "#{target}.app.dSYM.zip",
      destinations: destination
    )

    build_info = Actions.lane_context[Actions::SharedValues::APPCENTER_BUILD_INFORMATION]
    puts build_info
    build_base_url = ENV['APPCENTER_RELEASE_LINK']
    build_url = "#{build_base_url}#{build_info['id']}"
    puts build_url

    issue = ENV['CI_ISSUE_KEY']
    build_number = build_number_for_branch(target)

    post_build_to_slack(target, build_number, build_url, release_notes, nil)

    if !issue.nil?
      # post comment
      comment = ['Build is ready:']
      comment = ['WIP is ready:'] if is_wip_merge_request()
      comment << "[#{build_number}|#{build_url}]"
      comment << ''
      comment << 'Release Notes:'
      comment << release_notes.lines
      comment = comment.flatten
      comment = comment.map { |entry| entry.sub(issue, '').strip() }
      comment = comment.join("\n")
      post_jira_comment(issue, comment)
    end

    upload_dsyms()
  end


  #
  # Deliver
  #

  def release_build_appcenter(target, scheme)
    ipa_name = "#{scheme}.ipa"

    xcode_select ENV['XCODE_APP']

    match(type: 'adhoc', readonly: true, force_for_new_devices: true)
    update_provisions(target, 'adhoc')

    sh("mkdir -p #{ENV['ARCHIVES_DIR']}")
    sh("mkdir -p #{ENV['DERIVED_DATA_DIR']}")

    cocoapods(try_repo_update_on_error: true)

    gym(
      derived_data_path: ENV['DERIVED_DATA_DIR'],
      build_path: ENV['ARCHIVES_DIR'],
      scheme: scheme,
      export_options: {
        method: 'ad-hoc',
        iCloudContainerEnvironment: 'Production'
      },
      output_name: ipa_name
    )
  end

  def release_deploy_appcenter(target, scheme, browse_url)
    destination = ENV['APPCENTER_GROUP']

    release_notes = changelog_for_ios(target)

    appcenter_upload(
      release_notes: release_notes,
      notify_testers: true,
      file: "#{scheme}.ipa",
      dsym: "#{scheme}.app.dSYM.zip",
      destinations: destination
    )

    direct_url = Actions.lane_context[Actions::SharedValues::APPCENTER_BUILD_INFORMATION]['install_url']
    direct_url = direct_url.gsub('itms-services://', 'itms-services://test')

    build_number = get_version_number(target: target)

    post_build_to_slack(target, build_number, direct_url, release_notes, nil)

    upload_dsyms()
  end

  def release_build_appstore(target, scheme)
    ipa_name = "#{scheme}_appstore.ipa"

    archive_path = Dir["#{ENV['ARCHIVES_DIR']}/**/*.xcarchive"].map{ |f| File.absolute_path f }[0]

    match(type: 'appstore')
    update_provisions(target, 'appstore')

    gym(
      derived_data_path: ENV['DERIVED_DATA_DIR'],
      archive_path: archive_path,
      skip_build_archive: true,
      scheme: scheme,
      export_options: {
        method: 'app-store',
        iCloudContainerEnvironment: 'Production'
      },
      output_name: ipa_name
    )

    upload_dsyms()
  end

  def release_submit_appstore(target, scheme)
    ipa_name = "#{scheme}_appstore.ipa"

    pilot(
      ipa: ipa_name,
      skip_waiting_for_build_processing: true
    )
  end

  def upload_dsyms()
    gsp_path = ENV['GOOGLE_SERVICES_PLIST']
    upload_symbols_to_crashlytics(gsp_path: gsp_path) if !gsp_path.nil?
  end


  #
  # Info
  #

  def build_number_for_branch(target)
    version = get_version_number(target: target)
    destination = ENV['APPCENTER_GROUP']
    pipeline_iid = ENV['CI_PIPELINE_IID']

    case destination.downcase
    when 'internal'
      return "#{version}.#{pipeline_iid}"
    when 'testing'
      jira_task_id = ENV['CI_ISSUE_KEY']
      build_number = "#{version}"
      build_number += ".#{jira_task_id}" if !jira_task_id.nil?
      build_number += ".#{pipeline_iid}" if !pipeline_iid.nil?
      return build_number
    else
      return nil
    end
  end

  def changelog_for_ios(target)
    case ENV['APPCENTER_GROUP'].downcase
    when 'internal'
      unreleased_changelog(nil)
    when 'release'
      version = get_version_number(target: target)
      version_changelog(version)
    else
      unreleased_changelog(ENV['CI_ISSUE_KEY'])
    end
  end


  #
  # Badge
  #

  def fit_ios_badge(version)
    shield = shield_for_ios_badge(version)
    add_badge(shield: shield, no_badge: true) if !shield.nil?
  end

  def shield_for_ios_badge(version)
    # convert EW-3232 to EW3232
    regex = /([a-zA-Z0-9])/
    jira_task_id = ENV['CI_ISSUE_KEY']
    if !jira_task_id.nil?
      jira_task_id = jira_task_id.scan(regex).join('')
    end

    destination = ENV['APPCENTER_GROUP']
    pipeline_iid = ENV['CI_PIPELINE_IID']

    case destination.downcase
    when 'testing'
      return "#{jira_task_id}-#{pipeline_iid}-lightgrey"
    when 'internal'
      return "beta-#{version}.#{pipeline_iid}-lightgrey"
    else
      return nil
    end
  end
end

#
# Android
#

platform :android do
  #
  # Lanes
  #

  desc 'Run lint on selected flavor'
  lane :ci_quality do |options|
    gradle(task: options[:task])

    report_path = "#{ENV['PWD']}/app/build/reports"
    new_report_path = "#{ENV['PWD']}/reports"

    sh("mkdir -p #{new_report_path}")
    sh("cp -r #{report_path}/* #{new_report_path}")
  end

  desc 'Run all unit tests'
  lane :ci_tests do |options|
    gradle(task: options[:task], flags: "--stacktrace")
  end

  desc 'Run tests on AWS device farm'
  lane :ci_aws_tests do |options|
    gradle(task: options[:build_apk_task])
    apk_path = Actions.lane_context[Actions::SharedValues::GRADLE_APK_OUTPUT_PATH]
    puts "generated apk: #{apk_path}"

    gradle(task: options[:build_test_task])
    apk_test_path = Actions.lane_context[Actions::SharedValues::GRADLE_APK_OUTPUT_PATH]
    puts "generated android test apk: #{apk_test_path}"

    ENV['AWS_ACCESS_KEY_ID']     = 'AKIAZP4RB4UVPR5G75JN'
    ENV['AWS_SECRET_ACCESS_KEY'] = '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'
    ENV['AWS_REGION']            = 'us-west-2'

    aws_device_farm(
      name: 'appewa',
      device_pool: options[:device_pool],
      allow_failed_tests: 'true',
      print_web_url_of_run: 'true',
      binary_path: apk_path,
      test_binary_path: apk_test_path
    )

    if File.exist?(apk_path)
        File.delete(apk_path)
    end
    if File.exist?(apk_test_path)
        File.delete(apk_test_path)
    end

    puts "Report project arn: #{ENV['AWS_DEVICE_FARM_PROJECT_ARN']}"
    puts "Report run arn: #{ENV['AWS_DEVICE_FARM_RUN_ARN']}"
    puts "Report run url: #{ENV['AWS_DEVICE_FARM_WEB_URL_OF_RUN']}"

    if ENV['AWS_DEVICE_FARM_WEB_URL_OF_RUN'].nil?
        exit(1)
    end

    sh("aws configure set aws_access_key_id #{ENV['AWS_ACCESS_KEY_ID']}")
    sh("aws configure set aws_secret_access_key #{ENV['AWS_SECRET_ACCESS_KEY']}")
    sh("aws configure set region #{ENV['AWS_REGION']}")

    run_response = sh("aws devicefarm get-run --arn #{ENV['AWS_DEVICE_FARM_RUN_ARN']}")
    report = parse_aws_run_response(run_response, ENV['AWS_DEVICE_FARM_WEB_URL_OF_RUN'])
    save_aws_device_farm_report(report)
  end

  desc 'Ensure that changelog entries are ready'
  lane :ci_changelog do
    notes = changelog_for_android()
    if notes.nil?
      puts('Add changelog entries to CHANGELOG.md!')
      puts('Use [Unreleased] section for testing or internal builds.')
      puts('Use [<version aka future release tag>] section for release builds.')
      exit(1)
    end
    puts "\nRelease notes:\n#{notes}"
  end

  desc 'Build Android app using provided parameters'
  lane :ci_build_app do |options|
    exit_if_not_ci()

    build_android_app(
      options[:app],
      options[:flavor],
      options[:build_type],
      options[:version_name]
    )
  end

  desc 'Deploy build, for testing or internal use only'
  lane :ci_deploy_app do |options|
    exit_if_not_ci()
    notes = changelog_for_android()
    puts "\nRelease notes:\n#{notes}"
    aws_test_report = read_aws_device_farm_report()
    puts "\nAWS test report:\n#{aws_test_report}"
    deploy_android_to_appcenter(notes, ENV['APPCENTER_GROUP'], aws_test_report)
  end

  desc 'Deliver build, for releases only'
  lane :ci_deliver_app do |options|
    exit_if_not_ci()
    notes = changelog_for_android()
    puts "\nRelease notes:\n#{notes}"
    aws_test_report = read_aws_device_farm_report()
    puts "\nAWS test report:\n#{aws_test_report}"
    deploy_android_to_appcenter(notes, ENV['APPCENTER_GROUP'], aws_test_report)
  end


  #
  # Build
  #

  def build_android_app(app, flavor, build_type, version_name)
    isRelease = (build_type=="Release" ? true : false)
    if isRelease
      gradle(
        task: "clean"
      )
      flags="--no-daemon --stacktrace"
    else
      flags="--stacktrace"
    end

    gradle(
      task: "#{app}:assemble",
      flavor: flavor,
      build_type: build_type,
      properties: {
          'versionName' => version_name
      },
      flags:flags
    )

    project_name = ENV['APPCENTER_APP_NAME']
    project_path = "#{ENV['PWD']}/apks/#{project_name}"

    apk_path = Actions.lane_context[Actions::SharedValues::GRADLE_APK_OUTPUT_PATH]
    apk_new_path = "#{project_path}/#{version_name}.apk"

    puts(project_path)
    puts("cp \"#{apk_path}\" #{apk_new_path}")

    sh("mkdir -p #{project_path}")
    sh("cp \"#{apk_path}\" #{apk_new_path}")
  end


  #
  # Deploy
  #

  def deploy_android_to_appcenter(release_notes, destination, aws_test_report)
    uploads = []

    project_name = ENV['APPCENTER_APP_NAME']
    project_path = "#{ENV['PWD']}/apks/#{project_name}"

    apk_files = Dir["#{project_path}/*.apk"]
      .reject {|f| File.directory? f}
      .map{ |f| File.absolute_path f }

    for apk_file in apk_files do
      is_china = apk_file.downcase.include?('china')
      is_internal = destination.downcase == 'internal'

      appcenter_upload(
        api_token: ENV['APPCENTER_API_TOKEN'],
        owner_name: ENV['APPCENTER_OWNER_NAME'],
        app_name: is_china ? ENV['APPCENTER_CHINA_APP_NAME'] : ENV['APPCENTER_APP_NAME'],
        file: apk_file,
        destinations: destination,
        notify_testers: is_internal,
        release_notes: release_notes
      )

      download_link = Actions.lane_context[Actions::SharedValues::APPCENTER_DOWNLOAD_LINK]
      upload = {
        'apk_name' => File.basename(apk_file),
        'apk_file' => apk_file,
        'is_china' => is_china,
        'install_url' => download_link
      }
      puts "Upload finished:\n#{upload}"
      uploads << upload
    end

    issue = ENV['CI_ISSUE_KEY']
    build_number = ENV['CI_BUILD_APP_VERSION_NAME']
    post_build_to_slack('EWA-android', build_number, uploads, release_notes, aws_test_report)

    if !issue.nil?
      # post comment
      comment = ['Builds are ready:']
      comment = ['WIPs is ready:'] if is_wip_merge_request()
      comment << uploads.map { |i| "[#{i['apk_name']}|#{i['install_url']}]" }
      comment << ''
      comment << 'Release Notes:'
      comment << release_notes.lines
      comment << 'AWS test report:'
      comment << aws_test_report
      comment = comment.flatten
      comment = comment.map { |entry| entry.sub(issue, '').strip() }
      comment = comment.join("\n")
      post_jira_comment(issue, comment)
    end
  end

  #
  # Info
  #

  def changelog_for_android()
    case ENV['APPCENTER_GROUP'].downcase
    when 'internal'
      unreleased_changelog(nil)
    when 'release'
      version = ENV['CI_RELEASE_VERSION']
      version_changelog(version) if !version.nil?
    else
      unreleased_changelog(ENV['CI_ISSUE_KEY'])
    end
  end
end

#
# Common: Info
#

# Used to limit lanes for CI only execution.
def exit_if_not_ci()
  if ENV['CI'].nil?
    puts('CI is required, do not call manually!')
    exit(1)
  end
end

# Use it to check for WIP request.
def is_wip_merge_request()
  mr_title = ENV['CI_MERGE_REQUEST_TITLE']
  return false if mr_title.nil?
  return mr_title.include?('WIP:')
end

# Gets release notes to publish.
def get_release_notes()
  last_commit = ENV['CI_COMMIT_MESSAGE']
  return "Last commit message:\n#{last_commit}" if !last_commit.nil?
  return 'No release notes'
end


#
# Common: JIRA
#

# Post comment to JIRA-issue with DOC format using JIRA_TOKEN.
def post_jira_comment(issue, comment)
  jira_post_request "/issue/#{issue}/comment", {
    "type" => "doc",
    "version" => "1",
    "body" => comment
  }
end

def jira_post_request(path, json)
  uri = URI.parse("https://lithiumlab.atlassian.net/rest/api/latest#{path}")
  request = Net::HTTP::Post.new(uri)
  auth = ENV['JIRA_TOKEN'].split(':')
  request.basic_auth(auth[0], auth[1])
  request.content_type = "application/json"
  request.body = JSON.dump(json)
  req_options = {
    use_ssl: uri.scheme == "https",
  }
  response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
    http.request(request)
  end
end

def jira_put_request(path, json)
  uri = URI.parse("https://lithiumlab.atlassian.net/rest/api/latest#{path}")
  request = Net::HTTP::Put.new(uri)
  auth = ENV['JIRA_TOKEN'].split(':')
  request.basic_auth(auth[0], auth[1])
  request.content_type = "application/json"
  request.body = JSON.dump(json)
  req_options = {
    use_ssl: uri.scheme == "https",
  }
  response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
    http.request(request)
  end
end

#
# Common: report from aws device farm
#
def parse_aws_run_response(run_response, run_url)
    runReportJson = JSON.parse(run_response)["run"]
    if !runReportJson.nil?
        report = ['result: ' + runReportJson["result"]]
        report << 'status: ' + runReportJson["status"]
        report << 'created: ' + runReportJson["created"]
        report << 'counters: ' + JSON.dump(runReportJson["counters"])
        report << 'arn: ' + runReportJson["arn"]
        report << 'run_url: ' + run_url
        report = report.flatten
        report = report.join("\n")
    end
    return report
end

def save_aws_device_farm_report(report)
    project_reports_path = "#{ENV['PWD']}/reports"
    report_path = "#{project_reports_path}/aws_device_farm_report"

    sh("mkdir -p #{project_reports_path}")
    if File.exist?(report_path)
        File.delete(report_path)
    end
    File.open(report_path, 'w') do |file|
        file.write(report)
    end
end

def read_aws_device_farm_report()
    project_reports_path = "#{ENV['PWD']}/reports"
    report_path = "#{project_reports_path}/aws_device_farm_report"
    if File.exist?(report_path)
        report = File.read(report_path)
    end
    if report.nil?
        report = 'skipped'
    end
    return report
end


#
# Common: Changelog
#

def unreleased_changelog(filter)
  changelog('Unreleased', filter)
end

def version_changelog(version)
  changelog(version, nil)
end

def changelog(section, filter)
  entries = changelog_entries(section, filter)
  return nil if entries.size == 0
  entries
    .map { |entry| entry.sub('###','').strip() }
    .join("\n")
end

def changelog_entries(section, filter)
  changelog_entries_by_groups(section, filter)
    .flatten
    .flatten
end

def changelog_entries_by_groups(section, filter)
  changelog = read_changelog(
    changelog_path: ENV['CHANGELOG_PATH'],
    section_identifier: "[#{section}]",
    excluded_markdown_elements: []
  )

  last_group = nil
  has_filter = !filter.nil? && !filter.empty?

  changelog.lines
    .map { |entry| entry.strip() }
    .select { |entry| !entry.empty? }
    .select { |entry| entry.start_with?('###') || !has_filter || entry.include?(filter) }
    .reduce({}) { |result, entry|
      if entry.start_with?('###')
        last_group = entry
      else
        result[last_group] = [] if !result.has_key?(last_group)
        result[last_group] << entry
      end
      result
    }
end


#
# Common: Slack
#

# Post message to Slack with install URL and release notes.
def post_build_to_slack(target, build_number, install_url, release_notes, aws_test_report)
  fields = []
  jira_task_id = ENV['CI_ISSUE_KEY']
  jira_task_url = ENV['CI_ISSUE_URL']
  if !jira_task_id.nil? && !jira_task_url.nil?
    fields << {
      title: 'Task:',
      value: slack_url(jira_task_url, jira_task_id),
      short: true
    }
  end

  merge_request_url = ENV['CI_MERGE_REQUEST_URL']
  if !merge_request_url.nil?
    fields << {
      title: 'M/R:',
      value: slack_url(merge_request_url, 'View request'),
      short: true
    }
  end

  pipeline_url = ENV['CI_PIPELINE_URL']
  if !pipeline_url.nil?
    fields << {
      title: 'Pipeline:',
      value: slack_url(pipeline_url, 'View pipeline'),
      short: true
    }
  end

  if !install_url.nil?
    if install_url.is_a? Array
      url_list = 'Select app to install:'
      for item in install_url
        item_url = slack_url(item['install_url'], item['apk_name'])
        url_list += "\n#{item_url}"
      end
      fields << {
        title: 'Install urls:',
        value: url_list,
        short: false
      }
    else
      fields << {
        title: 'Install url:',
        value: slack_url(install_url, 'Click to install app'),
        short: false
      }
    end
  end

  if !release_notes.nil?
    fields << {
      title: 'Release notes:',
      value: release_notes,
      short: false
    }
  end

  if !aws_test_report.nil?
    fields << {
      title: 'AWS test report:',
      value: aws_test_report,
      short: false
    }
  end

  destination = ENV['APPCENTER_GROUP']

  color = '#F0F0F0'
  title = "#{target} build #{build_number}"
  if is_wip_merge_request()
    color = '#FFDD00'
    title = "#{target} build #{build_number} [WIP]"
  end

  slack(
    username: target,
    default_payloads: [],
    attachment_properties: {
      fields: fields,
      color: color,
      title: title,
      text: "Deployed to #{ENV['APP_NAME']} -> #{destination}",
      fallback: "#{target} build #{build_number} deployed to #{ENV['APP_NAME']} -> #{destination}, click to install app: #{install_url}"
    }
  )
end

# Shortcut for slack URL format.
def slack_url(url, title)
  "<#{url}|#{title}>"
end
